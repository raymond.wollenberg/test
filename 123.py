import numpy as np
import matplotlib.pyplot as plt
import scipy.linalg
from mpl_toolkits.mplot3d import Axes3D
import scipy.stats as scs



data = np.genfromtxt('D:\Homeoffice\Digipark\Genauigkeitsuntersuchung\Patches\Zugeschnitten\Zeb_Schnell_Patch7 - Cloud.txt', delimiter=' ')
data2 = data[:, 7]
Cx = data[:, 0]
Cy = data[:, 1]
Cz = data[:, 2]

#plt.hist(data2, bins="auto")
#plt.show()

X, Z = np.meshgrid(np.arange(min(Cx)-0.5, max(Cx)+0.5, 0.1), np.arange(min(Cz)-0.5, max(Cz)+0.5, 0.1))

a = np.ones(5)
print(a)

A = np.c_[Cx, Cz, np.ones(Cy.shape[0])]
C, R, _, T = scipy.linalg.lstsq(A, Cy)



Y = C[0] * X + C[1] * Z + C[2]

print(R)

# fig = plt.figure()
# ax = fig.gca(projection='3d')
# ax.plot_surface(X, Y, Z, rstride=1, cstride=1, alpha=0.2)
# ax.scatter(Cx, Cy, Cz, c='r', s=5)
# plt.xlabel('X')
# plt.ylabel('Y')
# ax.set_zlabel('Z')
#plt.show()

v1 = [X[0][0], Y[0][0], Z[0][0]]
v2 = [X[1][0]-X[0][0], Y[1][0]-Y[0][0], Z[1][0]-Z[0][0]]
v3 = [X[0][1]-X[0][0], Y[0][1]-Y[0][0], Z[0][1]-Z[0][0]]
print(v1, v2, v3)
n = np.cross(v2,v3)
nn = np.linalg.norm(n)
nnn = n/nn

p = [Cx, Cy, Cz]

dist = np.dot(nnn, p)-np.dot(v1, nnn)

std = np.std(dist, axis=0)
mean = np.mean(dist)

#ax.scatter(p[0], p[1], p[2]);
#ax.quiver(*p, *nnn)
#plt.ylim(-60, -66)
#plt.xlim(-65, -71)
#plt.show()

print('Dot', np.dot(v2, nnn))
print(nnn,p)
print(dist)
print(data[1, 7])

hist, bin_edges = np.histogram(dist)

print(hist)
print(bin_edges)

fig, ax = plt.subplots()

histo, bins, patches = ax.hist(dist, bins=20, density=True)

gauss =  scs.norm.pdf(bins, mean, std)
plt.plot(bins, gauss, 'k', linewidth=2)

plt.title('My Very Own Histogram')


plt.grid('True')
text = 'Standardabweichung = '+ str(std.round(4)) + ' mm' + '\n' + 'Mittelwert = ' + '{:0.3e}'.format(mean) + ' mm'
# plt.text(0, 0, text)
ax.set_xlabel(text)

'{:0.3e}'.format(2.32432432423e25)

plt.savefig('plot2.png')
plt.show()
